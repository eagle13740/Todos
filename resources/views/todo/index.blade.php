<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>todos</title>

</head>
<body class="">
    <div class="list-group">
        @foreach( $todo as $item )
            <a href="todo/{{ $item->id }}" class="list-group-item list-group-item-action ">{{ $item->name }}</a>
        @endforeach
    </div>
</body>
</html>
