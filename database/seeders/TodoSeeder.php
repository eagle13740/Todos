<?php

namespace Database\Seeders;

use App\Models\Todo;
use Illuminate\Database\Seeder;

class TodoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Todo::create([
            'name' => 'create_project',
            'description' => 'i created a project :)'
        ]);
        Todo::create([
            'name' => 'create_ekhraj',
            'description' => 'i ekhrajed a project :)'
        ]);
    }
}
